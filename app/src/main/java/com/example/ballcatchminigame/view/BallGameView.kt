package com.example.ballcatchminigame.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.example.ballcatchminigame.R
import com.example.ballcatchminigame.util.Coordinate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.random.Random

class BallGameView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var speed = 1.0f

    private val playerBitmap = AppCompatResources.getDrawable(context, R.drawable.ic_goalkeeper)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private val ballBitmap = AppCompatResources.getDrawable(context, R.drawable.ic_football)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)

    private var playerWidth = 1.0f
    private var playerHeight = 1.0f
    private var pCoord = Coordinate()
    private var score = 0
    private var ballSize = 1.0f

    private var spawnTimer = 0

    private val balls = ArrayList<Coordinate>()

    companion object {
        const val STATE_ACTIVE = 0
        const val STATE_PAUSE = 1
    }

    private var state = STATE_ACTIVE

    private var mInterface: BallGameInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            speed = mHeight / 120.0f
            clipBounds = Rect(0,0,mWidth, mHeight)
            putPlayerOnField()
            ballSize = mWidth / 10.0f
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                if(state == STATE_ACTIVE) movePlayer(event.x)
                return true
            }
            MotionEvent.ACTION_UP -> {
                if(state == STATE_ACTIVE) movePlayer(event.x)
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == STATE_ACTIVE) updateBalls()
            drawBalls(it)
            drawPlayer(it)
        }
    }

    /// Public

    fun setInterface(i: BallGameInterface) {
        mInterface = i
    }

    fun restart() {
        speed = mHeight / 120.0f
        pCoord.x = mWidth / 2.0f
        balls.clear()
        score = 0
        mInterface?.updateScore(score)
        state = STATE_ACTIVE
    }

    fun start() {
        state = STATE_ACTIVE
    }

    fun pause() {
        state = STATE_PAUSE
    }

    /// Private

    private fun updateBalls() {
        val playerY = mHeight - playerHeight * 0.95f - ballSize / 2.0f
        val pLeft = pCoord.x - (playerWidth + ballSize) / 2.0f
        val pRight = pCoord.x + (playerWidth + ballSize) / 2.0f
        var n = 0
        while(n < balls.size && state == STATE_ACTIVE) {
            balls[n].y+=speed
            if(balls[n].y > playerY && balls[n].x in pLeft..pRight) {
                balls.removeAt(n)
                n--
                score++
                mInterface?.updateScore(score)
                speed+=0.03f
            } else if (balls[n].y > mHeight + ballSize / 2.0f) {
                state = STATE_PAUSE
                mInterface?.onEnd()
                break
            }
            n++
        }
        spawnTimer--
        if(spawnTimer <= 0) {
            spawnBall()
            spawnTimer = Random.nextInt(30, 90)
        }
    }

    private fun drawPlayer(c: Canvas) {
        c.drawBitmap(playerBitmap, null, Rect((pCoord.x - playerWidth / 2.0f).toInt(), (pCoord.y - playerHeight / 2.0f).toInt(), (pCoord.x + playerWidth / 2.0f).toInt(), (pCoord.y + playerHeight / 2.0f).toInt()), Paint())
    }

    private fun drawBalls(c: Canvas) {
        val p = Paint()
        val r = ballSize / 2.0f
        for(ball in balls) c.drawBitmap(ballBitmap, null, Rect((ball.x - r).toInt(), (ball.y - r).toInt(),(ball.x + r).toInt(),(ball.y + r).toInt()), p)
    }

    private fun spawnBall() {
        balls.add(Coordinate(Random.nextInt(ballSize.toInt() / 2, mWidth - ballSize.toInt() / 2).toFloat(), ballSize / 2))
    }

    private fun putPlayerOnField() {
        playerWidth = mWidth / 5.0f
        playerHeight = playerWidth / (playerBitmap.width.toFloat() / playerBitmap.height)
        pCoord.x = mWidth / 2.0f
        pCoord.y = mHeight - playerHeight / 2.0f
    }

    private fun movePlayer(x: Float) {
        pCoord.x = x.coerceIn(playerWidth / 2.0f, mWidth - playerWidth / 2.0f)
    }

    interface BallGameInterface {
        fun updateScore(score: Int)
        fun onEnd()
    }
}