package com.example.ballcatchminigame.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.ballcatchminigame.R
import com.example.ballcatchminigame.ui.theme.Transparent_black
import com.example.ballcatchminigame.ui.theme.Transparent_grey

@Composable
fun PausePopup(onPress: () -> Unit = {}) {
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onPress() }
        .background(color = Transparent_grey)
        .padding(horizontal = 20.dp), contentAlignment = Alignment.Center) {
        Image( painterResource(id = R.drawable.ic_baseline_pause_24),"",
            modifier = Modifier
                .wrapContentSize()
                .background(color = Transparent_black, shape = RoundedCornerShape(20.0f.dp))
                .padding(15.0f.dp)
                .border(shape = RoundedCornerShape(20.0f.dp), color = Color.White, width = 2.dp)
        )
    }
}