package com.example.ballcatchminigame.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    WEB_VIEW("web_view"),
    MAIN_SCREEN("main"),
    RECORDS_SCREEN("records"),
    GAME_SCREEN("game")
}