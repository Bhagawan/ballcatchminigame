package com.example.ballcatchminigame.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


val Blue_light = Color(0xFF466783)
val Blue_dark = Color(0xFF1F5B8F)
val Transparent_black = Color(0xCC000000)
val Transparent_grey = Color(0x80000000)
val Red = Color(0x805F2222)
val Gold = Color(0x80FFB500)