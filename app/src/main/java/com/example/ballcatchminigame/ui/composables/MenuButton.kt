package com.example.ballcatchminigame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.ballcatchminigame.ui.theme.Blue_dark
import com.example.ballcatchminigame.ui.theme.Blue_light

@Composable
fun MenuButton(text: String = "", modifier: Modifier = Modifier, action: () -> Unit = {}) {
    val iS = remember { MutableInteractionSource() }
    BoxWithConstraints(modifier = modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        val brush = Brush.radialGradient(
            0.0f to Blue_light,
            0.5f to Blue_dark,
            0.8f to Blue_dark,
            1.0f to Color.Transparent, radius = maxWidth.value
        )
        Box(modifier = modifier.clickable(iS, null) {
            action()
        }
            .background(brush, RoundedCornerShape(20.dp))
            .padding(vertical = 10.dp)) {
            Text(text = text, fontSize = 30.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.Center))
        }
    }
}