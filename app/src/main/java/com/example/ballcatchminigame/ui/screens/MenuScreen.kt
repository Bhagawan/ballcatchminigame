package com.example.ballcatchminigame.ui.screens

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.ballcatchminigame.R
import com.example.ballcatchminigame.ui.composables.MenuButton
import com.example.ballcatchminigame.util.Navigator
import com.example.ballcatchminigame.util.UrlBack

@Composable
fun MenuScreen() {
    Image(painter = rememberImagePainter(UrlBack), contentDescription = "back", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    val activity = LocalContext.current as? Activity
    Column(modifier = Modifier.fillMaxSize()) {
        Box(modifier = Modifier
            .weight(1.0f, true).padding(horizontal = 50.dp, vertical = 20.dp), contentAlignment = Alignment.Center) {
            MenuButton(stringResource(id = R.string.btn_game), action =  { Navigator.navigateTo(Screens.GAME_SCREEN)})
        }
        Box(modifier = Modifier
            .weight(1.0f, true).padding(horizontal = 50.dp, vertical = 20.dp), contentAlignment = Alignment.Center) {
            MenuButton(stringResource(id = R.string.btn_records), action =  { Navigator.navigateTo(Screens.RECORDS_SCREEN)})
        }
        Box(modifier = Modifier
            .weight(0.5f, true).padding(horizontal = 70.dp, vertical = 10.dp), contentAlignment = Alignment.Center) {
            MenuButton(stringResource(id = R.string.btn_exit), action =  { activity?.finish() })
        }
    }
}