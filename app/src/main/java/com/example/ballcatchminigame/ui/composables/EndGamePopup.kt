package com.example.ballcatchminigame.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.ballcatchminigame.R
import com.example.ballcatchminigame.ui.theme.Blue_light
import com.example.ballcatchminigame.ui.theme.Red
import com.example.ballcatchminigame.ui.theme.Transparent_black
import com.example.ballcatchminigame.ui.theme.Transparent_grey

@Preview
@Composable
fun EndGamePopup(time: Int =0, score: Int =0, onExit: () -> Unit ={}, onRestart: () -> Unit ={}) {
    Box(modifier = Modifier
        .fillMaxSize()
        .background(color = Transparent_grey)
        .padding(horizontal = 50.dp), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .wrapContentSize()
                .background(color = Transparent_black, shape = RoundedCornerShape(20.0f.dp))
                .padding(5.0f.dp)
                .border(shape = RoundedCornerShape(20.0f.dp), color = Color.White, width = 2.dp)
                .padding(10.0f.dp)
        ) {
            Text(
                text = stringResource(id = R.string.str_end),
                fontSize = 50.sp,
                textAlign = TextAlign.Center,
                color = Color.White,
                modifier = Modifier.align(Alignment.CenterHorizontally))
            Text(
                text = stringResource(id = R.string.str_points, score),
                fontSize = 30.sp,
                textAlign = TextAlign.Center,
                color = Color.Yellow,
                modifier = Modifier.align(Alignment.CenterHorizontally))
            Text(
                text = stringResource(id = R.string.str_time, "${time / 60}:${time % 60}"),
                fontSize = 20.sp,
                textAlign = TextAlign.Center,
                color = Color.White,
                modifier = Modifier.align(Alignment.CenterHorizontally))
            Spacer(modifier = Modifier.height(20.dp))
            Row(modifier = Modifier.fillMaxWidth()) {
                Box(modifier = Modifier.weight(1.0f,true), contentAlignment = Alignment.Center) {
                    Image(painter = painterResource(id = R.drawable.ic_exit),
                        contentDescription = stringResource(id = R.string.desc_exit),
                        contentScale = ContentScale.Inside,
                        modifier = Modifier
                            .background(Red, RoundedCornerShape(20.dp))
                            .padding(10.dp)
                            .height(50.dp)
                            .clickable { onExit() })
                }
                Box(modifier = Modifier.weight(1.0f,true), contentAlignment = Alignment.Center) {
                    Image(painter = painterResource(id = R.drawable.ic_baseline_restart),
                        contentDescription = stringResource(id = R.string.desc_exit),
                        contentScale = ContentScale.Inside,
                        modifier = Modifier
                            .background(Blue_light, RoundedCornerShape(20.dp))
                            .padding(10.dp)
                            .height(50.dp)
                            .clickable { onRestart() })
                }
            }
        }
    }
}