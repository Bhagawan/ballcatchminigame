package com.example.ballcatchminigame.ui.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer

class BallGameScreenViewModel: ViewModel() {
    private val  _timeFlow = MutableStateFlow(0)
    val timeFlow = _timeFlow.asStateFlow()

    private val  _pointsFlow = MutableStateFlow(0)
    val pointFlow = _pointsFlow.asStateFlow()

    private val  _endPopup = MutableStateFlow(false)
    val endPopup = _endPopup.asStateFlow()

    private val  _pausePopup = MutableStateFlow(false)
    val pausePopup = _pausePopup.asStateFlow()

    private val  _restartGame = MutableSharedFlow<Boolean>()
    val restartGame = _restartGame.asSharedFlow()

    init {
        fixedRateTimer("time", false, 0, 1000) {
            viewModelScope.launch {
                if(!pausePopup.value && ! endPopup.value) _timeFlow.emit(timeFlow.value + 1)
            }
        }
    }

    fun setPointsAmount(amount: Int) {
        _pointsFlow.tryEmit(amount)
    }

    fun pause(active: Boolean) {
        _pausePopup.tryEmit(active)
    }

    fun restartGame() {
        _endPopup.tryEmit(false)
        _timeFlow.tryEmit(0)
        viewModelScope.launch {
            _restartGame.emit(true)
        }
    }

    fun endGame() {
        _endPopup.tryEmit(true)
    }
}