package com.example.ballcatchminigame.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.ballcatchminigame.R
import com.example.ballcatchminigame.ui.theme.Blue_light
import com.example.ballcatchminigame.ui.theme.Gold
import com.example.ballcatchminigame.ui.theme.Transparent_grey
import com.example.ballcatchminigame.util.Navigator
import com.example.ballcatchminigame.util.SavedPrefs
import com.example.ballcatchminigame.util.UrlBack

@Preview
@Composable
fun RecordsScreen() {
    Image(painter = rememberImagePainter(UrlBack), contentDescription = "back", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    val context = LocalContext.current
    val records = remember { SavedPrefs.getRecords(context).sortedByDescending { it.score } }
    Column(modifier = Modifier
        .fillMaxSize()
        .background(Transparent_grey)
        .padding(20.dp)) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                Button(
                    onClick = { Navigator.navigateTo(Screens.MAIN_SCREEN) },
                    shape = RoundedCornerShape(10.dp),
                    elevation = ButtonDefaults.buttonElevation(defaultElevation = 1.dp, pressedElevation = 0.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = Blue_light, contentColor = Color.White)
                ) {
                    Image(painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24), contentDescription = stringResource(
                        id = R.string.desc_exit),
                    contentScale = ContentScale.Inside,)
                }
            }
            Text(text = stringResource(id = R.string.str_records),
                textAlign = TextAlign.Center,
                fontSize = 40.sp,
                color = Color.White,
                modifier = Modifier
                    .weight(3.0f, true))
            Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {

            }
        }
        Column(verticalArrangement = Arrangement.spacedBy(4.dp),modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())) {
            for(record in records.withIndex()) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(3.dp)
                    .background(Blue_light, RoundedCornerShape(5.dp))) {
                    val n = record.index + 1
                    Text(text = n.toString(),
                        textAlign = TextAlign.Center,
                        fontSize = 20.sp,
                        color = if(n < 4) Gold else Color.White,
                        modifier = Modifier.weight(1.0f, true))

                    Text(text = record.value.score.toString(),
                        textAlign = TextAlign.Center,
                        fontSize = 20.sp,
                        color = if(n < 4) Gold else Color.White,
                        modifier = Modifier.weight(5.0f, true))

                    Text(text = record.value.time,
                        textAlign = TextAlign.Center,
                        fontSize = 20.sp,
                        color = if(n < 4) Gold else Color.White,
                        modifier = Modifier.weight(2.0f, true))
                }
            }
        }
    }
}
