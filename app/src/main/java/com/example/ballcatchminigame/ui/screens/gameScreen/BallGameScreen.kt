package com.example.ballcatchminigame.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.ballcatchminigame.R
import com.example.ballcatchminigame.ui.composables.EndGamePopup
import com.example.ballcatchminigame.ui.composables.PausePopup
import com.example.ballcatchminigame.ui.screens.Screens
import com.example.ballcatchminigame.ui.theme.Transparent_black
import com.example.ballcatchminigame.util.Navigator
import com.example.ballcatchminigame.util.Record
import com.example.ballcatchminigame.util.SavedPrefs
import com.example.ballcatchminigame.util.UrlBack
import com.example.ballcatchminigame.view.BallGameView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun BallGameScreen() {
    val viewModel = viewModel(BallGameScreenViewModel::class.java)
    val pointsAmount by viewModel.pointFlow.collectAsState(initial = 0)
    val context = LocalContext.current
    Image(painter = rememberImagePainter(UrlBack), contentDescription = "back", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    val vieww = remember{
        BallGameView(context).apply {
            setInterface(object : BallGameView.BallGameInterface {
                override fun updateScore(score: Int) {
                    viewModel.setPointsAmount(score)
                }
                override fun onEnd() {
                    val t = viewModel.timeFlow.value
                    SavedPrefs.saveRecord(context, Record(pointsAmount, "${t / 60}:${t % 60}"))
                    viewModel.endGame()
                }
            })
            viewModel.restartGame.onEach { restart() }.launchIn(viewModel.viewModelScope)
            viewModel.pausePopup.onEach {
                if(it) pause() else start()
            }.launchIn(viewModel.viewModelScope)
        }
    }
    AndroidView(factory = { vieww },
        modifier = Modifier.fillMaxSize(),
        update = {})
    Row(modifier = Modifier
        .fillMaxWidth()
        .height(60.dp)
        .background(Transparent_black)
        .padding(5.dp),
        verticalAlignment = Alignment.CenterVertically) {
        Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {

            val time by viewModel.timeFlow.collectAsState(initial = 0)
            Text(
                text = "${time / 60}:${time % 60}",
                fontSize = 25.sp,
                textAlign = TextAlign.Center,
                color = Color.White)
        }
        Box(modifier = Modifier.weight(2.0f, true), contentAlignment = Alignment.Center) {
            Text(
                text = stringResource(id = R.string.str_points, pointsAmount),
                fontSize = 30.sp,
                textAlign = TextAlign.Center,
                color = Color.White)
        }
        Box(modifier = Modifier
            .weight(1.0f, true)
            .clickable { viewModel.pause(true) }, contentAlignment = Alignment.Center) {
            Image(painter = painterResource(id = R.drawable.ic_baseline_pause_24),
                contentDescription = stringResource(id = R.string.desc_pause),
            modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Inside)
        }
    }

    val pausePopup by viewModel.pausePopup.collectAsState()
    val endPopup by viewModel.endPopup.collectAsState()
    if(endPopup) {
        val time = viewModel.timeFlow.value
        EndGamePopup(time = time, score = pointsAmount, onExit = { Navigator.navigateTo(Screens.MAIN_SCREEN)}, onRestart = viewModel::restartGame )
    }
    else if(pausePopup) PausePopup { viewModel.pause(false) }
}