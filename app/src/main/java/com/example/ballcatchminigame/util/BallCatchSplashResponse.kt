package com.example.ballcatchminigame.util

import androidx.annotation.Keep

@Keep
data class BallCatchSplashResponse(val url : String)