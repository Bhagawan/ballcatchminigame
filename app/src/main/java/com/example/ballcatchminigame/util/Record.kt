package com.example.ballcatchminigame.util

data class Record(val score: Int, val time: String)
