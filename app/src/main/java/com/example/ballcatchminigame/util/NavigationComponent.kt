package com.example.ballcatchminigame.util

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.ballcatchminigame.ui.screens.*
import com.example.ballcatchminigame.ui.screens.gameScreen.BallGameScreen
import im.delight.android.webview.AdvancedWebView
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, orientationLock: (l: Boolean) -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            SplashScreen()
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.onBackPressed()) exitProcess(0)
            }
            WebViewScreen(webView, remember { CurrentAppData.url })
        }
        composable(Screens.MAIN_SCREEN.label) {
            BackHandler(true) {}
            MenuScreen()
        }
        composable(Screens.GAME_SCREEN.label) {
            BackHandler(true) {}
            BallGameScreen()
        }
        composable(Screens.RECORDS_SCREEN.label) {
            BackHandler(true) {}
            RecordsScreen()
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    if(currentScreen == Screens.GAME_SCREEN) orientationLock(true)
    navController.navigate(currentScreen.label)

    navController.enableOnBackPressed(true)
}