package com.example.ballcatchminigame.util

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class SavedPrefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("BallCatchMiniGame", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

        fun saveRecord(context: Context, record: Record) {
            val shP = context.getSharedPreferences("BallCatchMiniGame", Context.MODE_PRIVATE)
            val g = Gson()
            val type = object : TypeToken<List<Record>>() {}.type
            val records = ArrayList(g.fromJson<List<Record>>(shP.getString("records", "[]"), type))
            records.add(record)

            shP.edit()
                .putString("records", g.toJson(records
                    .sortedByDescending { it.score }
                    .slice(0..100.coerceAtMost(records.size - 1))
                    .toList()))
                .apply()
        }

        fun getRecords(context: Context): List<Record> {
            val shP = context.getSharedPreferences("BallCatchMiniGame", Context.MODE_PRIVATE)
            val g = Gson()
            val type = object : TypeToken<List<Record>>() {}.type
            return g.fromJson(shP.getString("records", "[]"), type)
        }
    }
}